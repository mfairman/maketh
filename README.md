# maketh
Using `gnu make` to build programs and their external
dependencies, for host or cross-compile platforms.

Creative Commons (CC BY) Michael Fairman <mfairman@tegimeki.com>

## Usage
Your program's Makefile includes `maketh.mk` which
defines a number of functions helpful in guiding
a build.  This includes: management of an 
architecture-specific output directory where the
build takes place (so that your source tree stays
clean), the ability to specify dependencies at a
macro "thing" level, and various utilities for 
common build steps.

## Installation
Put maketh in your include-path:

    ln -s $PWD /usr/local/include  # (run from this repo's root)

or, symlink maketh/ into your project directories
case-by-case.

You may also simlink just the maketh.mk file if you
don't need the added vendor things.

## TBD
* support fetching of external libraries
* support non-native builds (cmake, etc.)
* support other patterns besides .cpp.o
* integrate original multi-arch build flow (see `target/`)
