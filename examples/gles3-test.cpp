#include <GLES3/gl3.h>
#include <iostream>

int main(int, char**)
{
    std::cout << "Built with OpenGLES3" << std::endl;
    glClearColor( 0, 0, 0, 0 );
    return 0;
}
