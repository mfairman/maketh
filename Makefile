#
# This Makefile demonstrates how to use maketh
#    (https://github.com/tegimeki/maketh)
#
# There are three steps:
#
# (1) include it
include maketh.mk


# (2) define build setup and targets
define build

# (2a) optional: specify variables that become -D<var>=$(<var>)
$(call maketh-define,OPTION) # e.g.: OPTION=3 => -DOPTION=3

# (2b) an example of an 'all' target, add any others here
#      (observe second-stage evaluation using $$)
all:
	@echo "SRCDIR=$(SRCDIR)" # where the original Makefile was
	@echo "OUTDIR=$(OUTDIR):"
	@ls -l bin
	@date > $$@

# (2c) an example of a custom target
custom:
	touch $$@

endef

# (3) maketh!
$(eval $(maketh))


