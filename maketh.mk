#
# maketh - make things
#
# https://github.com/tegimeki/maketh
#
# See example Makefile for usage
#

# bootstrap: figure out where we are included from
maketh-mk  := $(abspath $(lastword $(MAKEFILE_LIST)))
maketh-dir := $(dir $(maketh-mk))

# quiet by default; pass Q= to see build commands
Q ?= @

# if building in source directory, create output directory and build there
ifeq (,$(filter $(OUTDIR),$(CURDIR))$(filter $(OUTDIR),$(notdir $(CURDIR))))

# determine output directory
ifneq ($(CROSS),)
OUTDIR ?= .$(notdir $(CROSS))build
else
OUTDIR ?= .$(shell uname -p)-build
endif

define maketh-main

all:
	+@mkdir -p $(OUTDIR)
	+@echo "[NOTE]\tBuilding in $(OUTDIR)"
	+@$(MAKE) --no-print-directory -C $(OUTDIR) -I $(maketh-dir) \
		-f $(abspath $(firstword $(MAKEFILE_LIST))) \
		SRCDIR=$(CURDIR) OUTDIR=$(OUTDIR) $(MAKECMDGOALS)

.SUFFIXES:
.PHONY: $(OUTDIR) clean

$(OUTDIR): all
Makefile : ;

% :: $(OUTDIR) ; :


clean:
	@echo "[RM]\t$(OUTDIR)"
	@rm -rf $(OUTDIR)

endef



else # building in output directory: use caller's build function

VPATH = $(SRCDIR)

# todo: modularize the langauge-specific things
CXX = $(CROSS)g++
AR  = $(CROSS)ar
CXXFLAGS += -MD -MP -std=c++11

INCLUDES += .


define maketh-main

$(eval $(call build))

clean: ; :
.PHONY: clean

endef



# built-in rule for c++
.cpp.o:
	@echo "[CXX]\t$(notdir $<)"
	$(Q)mkdir -p $(dir $@)
	$(Q)$(CXX) $(CXXFLAGS) $(THING_CXXFLAGS) $(DEFINES) \
		$(foreach inc,$(filter-out /%,$(INCLUDES) $(THING_INCLUDES)),-I$(SRCDIR)/$(inc)) \
		$(foreach inc,$(filter /%,$(INCLUDES) $(THING_INCLUDES)),-I$(inc)) \
		-o $@ -c $<


# helper to add -D<var>=<value> when the given variable is set
# an additional argument can be passed with other C/CXX flags
define maketh-define

export $1
ifneq ($($1),)
DEFINES  += -D$1=$($1)
CXXFLAGS += $2
CFLAGS   += $2
$(if $($1),$(info [NOTE]  Setting -D$1=$($1) $(if $2,and $2,)))
endif

endef

# create targets for a thing
# $1 = thing name, $2 = .mk file path
define maketh-targets

$(if $($1-src),$(foreach src,$($1-src),$(eval $1-cxx += $(subst $(SRCDIR)/,,$(wildcard $(SRCDIR)/$(src)/*.cpp)))))
$(eval $1-obj += $($1-cxx:.cpp=.o))

ifeq ($(filter .%,$($1-bin)),) # buildable thing:
all: bin/$($1-bin)

$1: bin/$($1-bin)

$($1-obj): $(foreach dep,$($1-deps),bin/$($(dep)-bin))
bin/$($1-bin): THING_CXXFLAGS = $($1-cxxflags)
bin/$($1-bin): THING_INCLUDES = $($1-incs) $(foreach dep,$($1-deps),$($(dep)-incs))
bin/$($1-bin): $($1-obj) $($1-dfiles)
	$(Q)mkdir -p bin
ifeq ($(filter %.a,$($1-bin)),)
	@echo "[LD]\t$(notdir $$@)"
	$(Q)$(CXX) $($1-obj) \
		$$(filter %.a,$(foreach dep,$($1-deps),bin/$$($(dep)-bin)) $($1-libs)) -o $$@ \
		$(foreach lib,$(filter-out %.a,$($1-libs)),-l$(lib)) \
		$(foreach dep,$($1-deps),$(foreach lib,$(filter-out %.a,$($(dep)-libs)),-l$(lib))) \
		$(LDFLAGS) $($1-ldflags) \
		$(foreach dep,$($1-deps),$(foreach flag,$($(dep)-ldflags),$(flag)))
else
	@echo "[AR]\t$(notdir $$@)"
	$(Q)$(AR) cr $$@ $($1-obj) $$(filter %.a,$($1-libs))
endif

$1-dfiles = $$($1-cxx:.cpp=.d)
-include $$($1-dfiles)

else # host thing:
all: bin/$($1-bin)
$1: bin/$($1-bin)

bin/$($1-bin):
	$(Q)mkdir -p bin
	@echo "[HOST]\t$1"
	$(if $($1-libs),$(Q)touch $$@,@echo "[ERROR]\t$1 not available")
endif

endef

# library
define maketh-lib # $1=thing $2=incroot, $3=incsub, $4=libdir, $5=libname
ifneq ($(wildcard $2/$3),)
ifneq ($(wildcard $4/lib$5*.*),)
 $1-incs    += $2
 $1-ldflags += -L$4
 $1-libs    := $5
endif
endif
endef

define maketh-cxx # $1=thing, $2=bin, $3=cxx files, $4=deps, $5=libs
 $1-cxx   += $3
 $1-bin   := $2
 $1-deps  += $4
 $1-libs  += $5
endef

# include host things
MKFILES := $(sort $(wildcard $(maketh-dir)/host/*.mk))
-include $(MKFILES)
$(foreach h,$(MKFILES),$(eval $(basename $(notdir $(h)))-bin := .$(notdir $(h))))

# include app things
MKFILES := $(sort $(filter-out %host-%,$(wildcard $(SRCDIR)/*/*.mk)))
-include $(MKFILES)

$(foreach mkfile,$(MKFILES),\
 $(eval $(call maketh-targets,$(basename $(notdir $(mkfile))),$(mkfile))))


endif

# put   $(eval maketh)   at the end of your Makefile
maketh = $(call maketh-main)
