# -*- mode: makefile-gmake -*-
#
# This is a collection of GNU make rules handling the details of
# building/cross-compiling for multiple architectures.
#
# `build.rules` should be included by the project Makefile, after
# the TARGET variable is defined for the target system.  At the
# end of the project Makefile the `build` function should be
# evaluated, e.g. $(eval $(build)).  This will expand all of
# the definitions into actual targets for make to use.
#
# Code modules are specified by declaring variables which start
# with the module name and are suffixed by `-src` for directories
# containing source code, `-c`/`-cpp`/`-s` for individual source
# files, `-ld` for linker scripts, and `-lib` for references to
# libraries.  Once defined, a module can be included in the build
# by calling `add-app` or `add-lib` depending on whether an
# executable or statically-linked library (`.a`) is needed.
#
# There are two phases of the build:  the first is started when
# running `make` from the source directory, at which point an
# output build directory is created and then `make` is re-run
# in that directory to perform the actual compilation, linking,
# etc.  This scheme ensures all compilation and other file
# generation is done within the target output directory.
#
# With inspiration from:
#   https://make.mad-scientist.net/papers/multi-architecture-builds
#
# Creative Commons (CC BY) Michael Fairman <mfairman@tegimeki.com>

export TARGET ?= HOST

# host build environment settings
ifeq ($(OS),Windows_NT) # requires MSYS2 or equivalent
HOST_OS := win
HOST_CPUS ?= $(shell nproc)
MSG ?= @echo -e
else
ifeq ($(shell uname),Darwin)
HOST_OS := mac
HOST_CPUS ?= $(shell sysctl -n hw.physicalcpu)
MSG ?= @echo
else
HOST_OS := linux
HOST_CPUS ?= $(shell nproc)
MSG ?= @echo
endif
endif

# target
ifeq ($(CROSS),)
TARGET_OS ?= $(HOST_OS)
endif

# build is quiet by default; pass `Q=` for verbose command output
Q ?= @

# when initially run from the source directory, set-up the environment
# to re-run the build from the output directory
ifeq (,$(filter $(OUTDIR),$(CURDIR))$(filter $(OUTDIR),$(notdir $(CURDIR))))

# this is where builds go; override `OUTBASE` for absolute paths
# and/or `OUTDIR` for the subdirectory
OUTDIR ?= $(OUTBASE)$(if $(OUTBASE),/)BUILD-$(TARGET)$(if $(TARGET_OS),-$(TARGET_OS))

TOPDIR  := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

define build-main

all:
	+@mkdir -p $(OUTDIR)
	+$(MSG) "[NOTE]\tBuilding in $(abspath $(OUTDIR))" \
		$(if $(DEBUG),"[DEBUG]")
	+@$(MAKE) --no-print-directory -C $(OUTDIR) -I $(CURDIR) -I$(TOPDIR) \
		-f $(abspath $(firstword $(MAKEFILE_LIST))) \
		VERSION=$(VERSION) \
		SRCDIR=$(CURDIR) OUTDIR=$(OUTDIR) TOPDIR=$(TOPDIR) \
		$(MAKECMDGOALS)

.SUFFIXES:
.PHONY: $(OUTDIR) clean

$(OUTDIR): all
Makefile : ;

% :: $(OUTDIR) ; :

# clean runs in first phase only so is defined here:
clean:
	$(MSG) "[RM]\t$(OUTDIR)"
	@rm -rf $(OUTDIR)
.NOTPARALLEL: clean

endef


else 

# the second phase of the build runs in the output directory,
# so we set VPATH to find sources in project and from the
# top of the repo for relative paths to e.g. lib/...
VPATH = $(SRCDIR) $(TOPDIR)

# make sure we can run the compiler
ifneq ($(shell which $(CC) 2>&1 > /dev/null; echo $$?),0)
$(error Cannot run $(CC), check toolchain installation (CROSS=$(CROSS)))
endif

# get target-specific definitions
-include $(TARGET).mk

# toolchains and options
CXX = $(CROSS)g++$(GCCVER)
CC  = $(CROSS)gcc$(GCCVER)
LD  = $(CROSS)gcc$(GCCVER)
AS  = $(CROSS)gcc$(GCCVER)
AR  = $(CROSS)ar$(GCCVER)
OBJCOPY = $(CROSS)objcopy$(GCCVER)

CCSTD  ?= -std=gnu99
CXXSTD ?= -std=c++17

ifeq ($(DEBUG),1)
FLAGS += -g3 -DDEBUG
OLEVEL ?= -Og
else
OLEVEL ?= -O2
endif
FLAGS    += $(OLEVEL)

CFLAGS   += -DTARGET_$(TARGET)
CFLAGS   += -DTARGET_NAME=\"$(TARGET)\"

CFLAGS   += $(FLAGS)
CFLAGS   += -MD -MP
CFLAGS   += -Wall -Wno-unused-function -Wignored-qualifiers -Wno-format -Wno-psabi

CXXFLAGS += $(CFLAGS)

ASFLAGS += $(FLAGS)

INCFLAGS = $(foreach i,$(INCLUDES),-I$(SRCDIR)/$(i)) -I.


# relative location to top of tree, for global includes (e.g. `lib/`)
TOPREL := $(shell echo "$(subst $(TOPDIR),,$(SRCDIR))" | tr -s -c '/' '.' | sed -e 's/\./\.\./g')

define build-main

$(eval $(call main))

clean: ; :
.PHONY: clean

endef

# common compilation patterns
%.o: %.c
	$(MSG) "[CC]\t$(notdir $<)"
	$(Q)mkdir -p $(dir $@)
	$(Q)$(CC) $(CCSTD) $(CFLAGS) $(DEFINES) $(INCFLAGS) -o $@ -c $<

%.o: %.cpp
	$(MSG) "[CXX]\t$(notdir $<)"
	$(Q)mkdir -p $(dir $@)
	$(Q)$(CXX) $(CXXSTD) $(CXXFLAGS) $(DEFINES) $(INCFLAGS) -o $@ -c $<

%.o: %.S
	$(MSG) "[AS]\t$(notdir $<)"
	$(Q)mkdir -p $(dir $@)
	$(Q)$(AS) $(ASFLAGS) $(DEFINES) $(INCFLAGS) -o $@ -c $<

%.o: %.s
	$(MSG) "[AS]\t$(notdir $<)"
	$(Q)mkdir -p $(dir $@)
	$(Q)$(AS) $(ASFLAGS) $(DEFINES) $(INCFLAGS) -o $@ -c $<

define add-define

export $1
ifneq ($($1),)
DEFINES  += -D$1=$($1)
$(if $($1),$(info [NOTE]  Setting -D$1=$($1)))
endif

endef

# macros for generating targets

# $1 = target name, $2 = source dirs, $3 = file type
define add-src-dirs
$(foreach dir,$2,$(eval $1-$3 += $(subst $(TOPREL)/,,$(subst $(SRCDIR)/,,$(wildcard $(SRCDIR)/$(dir)/*.$3)))))
$(eval $1-obj += $($1-$3:.$3=.o))
endef


#
# targets for building a static library
#
# $1 = lib name
define add-lib
$(eval $(call lib-target,$1,$($1-src),$($1-inc)))
endef

# $1 = lib name, $2 = source dirs, $3 = inc dirs
define lib-target

$(eval $(call add-src-dirs,$1,$2,c))
$(eval $(call add-src-dirs,$1,$2,cpp))
$(eval $(call add-src-dirs,$1,$2,S))
$(eval $(call add-src-dirs,$1,$2,s))

all: $1
$1: $1.a

$1.a: ASFLAGS += $($1-asflags)
$1.a: CFLAGS += $($1-cflags)
$1.a: CXXFLAGS += $($1-cxxflags)
$1.a: INCLUDES += $(all-inc) $($1-inc)  \
	$(foreach lib,$($1-lib),$(foreach inc,$($(lib)-inc),$(inc)))
$1.a: $(foreach lib,$($1-lib),$(lib).a)
$1.a: $($1-obj) $($1-deps)
	$(MSG) "[AR]\t$(notdir $$@)"
	$(Q)$(AR) cr $$@ $($1-obj)

$1-deps = $$($1-c:.c=.d) $$($1-cpp:.cpp=.d) $$($1-S:.S=.d) $$($1-s:.s=.d)
-include $$($1-deps)

endef


#
# targets for building an executable
#
# $1 = app name, $2 = app type (empty: regular link, bin: elf2bin)
define add-app

$(eval $(call add-src-dirs,$1,$($1-src),c))
$(eval $(call add-src-dirs,$1,$($1-src),cpp))
$(eval $(call add-src-dirs,$1,$($1-src),S))
$(eval $(call add-src-dirs,$1,$($1-src),s))

# cross-compiled target application:
ifneq ($(TARGET),HOST)
all: $1.bin

$1: $1.bin
.PHONY: $1

$1.elf: ASFLAGS += $($1-asflags)
$1.elf: CFLAGS += $($1-cflags)
$1.elf: CXXFLAGS += $($1-cxxflags)
$1.elf: INCLUDES += $(all-inc) $($1-inc) $($1-src) \
	$(foreach lib,$($1-lib),\
		$(foreach inc,$(subst $(TOPDIR),$(TOPREL),$($(lib)-inc)),$(inc)))

$1.elf: $($1-obj) $($1-deps) $(foreach lib,$($1-lib),$(lib).a)
	$(MSG) "[LD]\t$(notdir $$@)"
	$(Q)$(LD) $($1-obj) $(foreach lib,$($1-lib),$(lib).a) -o $$@ \
		$(if $($1-ld),-T$(SRCDIR)/$($1-ld),\
			$(if $(LDFILE),-T$(SRCDIR)/$(LDFILE))) \
		-Xlinker -Map=$1.map \
		$(foreach ldflag,$(sort $(LDFLAGS)) $($1-ldflags),-Xlinker $(ldflag)) \
			$(FLAGS)

$1.bin: $1.elf
	$(MSG) "[BIN]\t$$@"
	$(Q)$(OBJCOPY) -O binary $$< $$@

else

# host-compiled application:
all: $1

$1: ASFLAGS += $($1-asflags)
$1: CFLAGS += $($1-cflags)
$1: CXXFLAGS += $($1-cxxflags)
$1: INCLUDES += $(all-inc) $($1-inc) $($1-src) \
	$(foreach lib,$($1-lib),\
		$(foreach inc,$(subst $(TOPDIR),$(TOPREL),$($(lib)-inc)),$(inc)))
$1: $($1-obj) $($1-deps) $(foreach lib,$($1-lib),$(lib).a)
	$(MSG) "[LD]\t$(notdir $$@)"
	$(Q)mkdir -p bin
	$(Q)$(LD) $($1-obj) $(foreach lib,$($1-lib),$(lib).a) -o $$@ \
		$(if $(LDFILE),-T$(SRCDIR)/$(LDFILE)) \
		$(foreach ldflag,$(LDFLAGS) $($1-ldflags),-Xlinker $(ldflag)) $(FLAGS)

run-$1: $1
	$(Q)./$1
endif

$1-deps = $$($1-c:.c=.d) $$($1-cpp:.cpp=.d) $$($1-S:.S=.d) $$($1-s:.s=.d)
-include $$($1-deps)

endef

endif # phase 2, building in $(OUTDIR)

build = $(call build-main) # eval this at end of main Makefile
